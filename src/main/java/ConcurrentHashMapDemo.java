import org.junit.jupiter.api.*;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Predicate;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class ConcurrentHashMapDemo {

    private Map map;
    private static Predicate<String> moreThanOneChar = s -> s.length() > 1;

    private void renovateEntry(Integer key, String value) {
        map.compute(key, (k,v) -> v == null ? value : (!moreThanOneChar.test(value) ? value : v));
    }

    @Test
    @Order(1)
    public void testMap() throws InterruptedException {
        map = new HashMap<Integer, String>();
        Thread thread1 = new Thread(() -> renovateEntry(0, "K"));
        Thread thread2 = new Thread(() -> renovateEntry(0, "QQ"));
        thread1.start();
        thread2.start();
        thread1.join();
        thread2.join();

        Assertions.assertEquals("K", map.get(0));
    }

    @Test
    @Order(2)
    public void testConcurrentMap() throws InterruptedException {
        map = new ConcurrentHashMap<Integer, String>();
        Thread thread1 = new Thread(() -> renovateEntry(0, "K"));
        Thread thread2 = new Thread(() -> renovateEntry(0, "QQ"));
        thread1.start();
        thread2.start();
        thread1.join();
        thread2.join();

        Assertions.assertEquals("K", map.get(0));
    }
}
